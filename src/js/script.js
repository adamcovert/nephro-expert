import ready from './utils/documentReady';

import {
  Swiper,
  Navigation,
  Pagination,
  Scrollbar,
  EffectFade,
  Autoplay
} from 'swiper';

import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

import Rellax from 'rellax';

ready(() => {

  const burger = document.querySelector('.s-page-header__burger');
  const mobileMenu = document.querySelector('.s-mobile-menu');
  const mobileMenuClose = document.querySelector('.s-mobile-menu__close');

  if (burger) {
    burger.addEventListener('click', function () {
      if (mobileMenu) {
        mobileMenu.classList.add('s-mobile-menu--is-active');
        disableBodyScroll(mobileMenu);
      }
    });
  };

  if (mobileMenuClose) {
    mobileMenuClose.addEventListener('click', function () {
      if (mobileMenu) {
        mobileMenu.classList.remove('s-mobile-menu--is-active');
        enableBodyScroll(mobileMenu);
      }
    });
  };

  Swiper.use([
    Navigation,
    Pagination,
    Scrollbar,
    EffectFade,
    Autoplay
  ]);

  const featuredTestimonialsSlider = new Swiper('.s-featured-testimonials__slider .swiper-container', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    slidesPerView: 1,
    speed: 1000,
    autoplay: {
      delay: 5000
    },
    loop: true,
    pagination: {
      el: '.s-featured-testimonials__slider-pagination',
      clickable: true
    }
  });

  const newsSlider = new Swiper('.s-news__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.s-news__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-news__slider-nav .s-slider-nav__btn--next'
    },
    pagination: {
      el: '.s-news__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      }
    }
  });

  const doctorsSlider = new Swiper('.s-doctors__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.s-doctors__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-doctors__slider-nav .s-slider-nav__btn--next'
    },
    pagination: {
      el: '.s-doctors__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });

  const promoSlider = new Swiper('.s-promo__slider .swiper-container', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    slidesPerView: 1,
    speed: 1000,
    autoplay: {
      delay: 7500
    },
    loop: true,
    navigation: {
      prevEl: '.s-promo__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-promo__slider-nav .s-slider-nav__btn--next'
    },
  });

  const certificatesSlider = new Swiper('.s-certificates__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.s-certificates__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-certificates__slider-nav .s-slider-nav__btn--next'
    },
    pagination: {
      el: '.s-certificates__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 4
      }
    }
  });

  const gallerySlider = new Swiper('.s-gallery__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    draggable: true,
    navigation: {
      prevEl: '.s-gallery__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-gallery__slider-nav .s-slider-nav__btn--next'
    },
    pagination: {
      el: '.s-gallery__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      }
    }
  });



  const rellax = new Rellax('.rellax');



});